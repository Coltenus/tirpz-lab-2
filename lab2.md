## Національний технічний університет України<br>“Київський політехнічний інститут ім. Ігоря Сікорського”

## Факультет прикладної математики<br>Кафедра системного програмування і спеціалізованих комп’ютерних систем


# Лабораторна робота №2<br>"Розширена робота з git"

## КВ-13 Яцков Максим

## Хід виконання роботи
### 1.  Зклонувати репозиторій для ЛР2 використавши локальний репозиторій від ЛР1 в якості "віддаленого". Якщо для ЛР1 було використано не унікальний репозиторій, чи з інших причин для ЛР2 було обрано інший проект -- зклонуйте ваш новий репозиторій з інтернету, створіть свою гілку lab1-branch із кількома комітами і тоді використайте цей репозиторій в якості локального для клонування в ЛР2.

```
> git clone file:///home/student/lab2/imgui imgui-lab2
Клонування в "imgui-lab2"..  
remote: Перерахування обʼєктів: 280, готово.  
remote: Підрахунок обʼєктів: 100% (280/280), готово.  
remote: Компресія обʼєктів: 100% (176/176), готово.  
remote: Всього 280 (дельта 99), повторно використано 269 (дельта 94), повторно використано пакунків 0  
Отримання об’єктів: 100% (280/280), 1.58 МіБ | 23.45 МіБ/с, готово.  
Розв’язання дельт: 100% (99/99), готово.
```

```
> git branch -a
* master  
 remotes/origin/HEAD -> origin/master  
 remotes/origin/master
```

###  2. Робота з ремоутами

```
> git remote -v
origin  file:///home/student/lab2/imgui (fetch)  
origin  file:///home/student/lab2/imgui (push)
```

```
> git remote add upstream https://github.com/ocornut/imgui.git
> git remote -v
origin  file:///home/student/lab2/imgui (fetch)  
origin  file:///home/student/lab2/imgui (push)  
upstream        https://github.com/ocornut/imgui.git (fetch)  
upstream        https://github.com/ocornut/imgui.git (push)
```

```
> git fetch upstream
remote: Enumerating objects: 51584, done.  
remote: Counting objects: 100% (51584/51584), done.  
remote: Compressing objects: 100% (11633/11633), done.  
remote: Total 51405 (delta 40182), reused 50831 (delta 39688), pack-reused 0  
Отримання об’єктів: 100% (51405/51405), 79.19 МіБ | 1.45 МіБ/с, готово.  
Розв’язання дельт: 100% (40182/40182), завершено з 124 локальними об’єктами.  
Від https://github.com/ocornut/imgui  
* [нова гілка]        docking                               -> upstream/docking  
* [нова гілка]        features/demo_input_owner_and_routing -> upstream/features/demo_input_owner_and_routing  
* [нова гілка]        features/potocpav-newer-lines-2       -> upstream/features/potocpav-newer-lines-2  
* [нова гілка]        features/premake5                     -> upstream/features/premake5  
* [нова гілка]        features/range_select                 -> upstream/features/range_select  
* [нова гілка]        features/shadows                      -> upstream/features/shadows  
* [нова гілка]        features/string_view                  -> upstream/features/string_view  
* [нова гілка]        features/tex_round_corners            -> upstream/features/tex_round_corners  
* [нова гілка]        master                                -> upstream/master  
* [новий тег]         v1.00                                 -> v1.00  
* [новий тег]         v1.01                                 -> v1.01  
* [новий тег]         v1.02                                 -> v1.02  
* [новий тег]         v1.03                                 -> v1.03  
* [новий тег]         v1.04                                 -> v1.04  
* [новий тег]         v1.05                                 -> v1.05  
* [новий тег]         v1.06                                 -> v1.06  
* [новий тег]         v1.07                                 -> v1.07  
* [новий тег]         v1.08                                 -> v1.08  
* [новий тег]         v1.09                                 -> v1.09  
* [новий тег]         v1.10                                 -> v1.10  
* [новий тег]         v1.11                                 -> v1.11  
* [новий тег]         v1.12                                 -> v1.12  
* [новий тег]         v1.13                                 -> v1.13  
* [новий тег]         v1.14                                 -> v1.14  
* [новий тег]         v1.15                                 -> v1.15  
* [новий тег]         v1.16                                 -> v1.16  
* [новий тег]         v1.16b                                -> v1.16b  
* [новий тег]         v1.17                                 -> v1.17  
* [новий тег]         v1.18                                 -> v1.18  
* [новий тег]         v1.19                                 -> v1.19  
* [новий тег]         v1.20                                 -> v1.20  
* [новий тег]         v1.30                                 -> v1.30  
* [новий тег]         v1.31                                 -> v1.31  
* [новий тег]         v1.32                                 -> v1.32  
* [новий тег]         v1.33                                 -> v1.33  
* [новий тег]         v1.33b                                -> v1.33b  
* [новий тег]         v1.34                                 -> v1.34  
* [новий тег]         v1.35                                 -> v1.35  
* [новий тег]         v1.36                                 -> v1.36  
* [новий тег]         v1.37                                 -> v1.37  
* [новий тег]         v1.38                                 -> v1.38  
* [новий тег]         v1.40                                 -> v1.40  
* [новий тег]         v1.41                                 -> v1.41  
* [новий тег]         v1.42                                 -> v1.42  
* [новий тег]         v1.43                                 -> v1.43  
* [новий тег]         v1.44                                 -> v1.44  
* [новий тег]         v1.45                                 -> v1.45  
* [новий тег]         v1.46                                 -> v1.46  
* [новий тег]         v1.47                                 -> v1.47  
* [новий тег]         v1.48                                 -> v1.48  
* [новий тег]         v1.49                                 -> v1.49  
* [новий тег]         v1.50                                 -> v1.50  
* [новий тег]         v1.51                                 -> v1.51  
* [новий тег]         v1.52                                 -> v1.52  
* [новий тег]         v1.53                                 -> v1.53  
* [новий тег]         v1.60                                 -> v1.60  
* [новий тег]         v1.61                                 -> v1.61  
* [новий тег]         v1.62                                 -> v1.62  
* [новий тег]         v1.63                                 -> v1.63  
* [новий тег]         v1.64                                 -> v1.64  
* [новий тег]         v1.65                                 -> v1.65  
* [новий тег]         v1.66                                 -> v1.66  
* [новий тег]         v1.66b                                -> v1.66b  
* [новий тег]         v1.67                                 -> v1.67  
* [новий тег]         v1.68                                 -> v1.68  
* [новий тег]         v1.69                                 -> v1.69  
* [новий тег]         v1.70                                 -> v1.70  
* [новий тег]         v1.71                                 -> v1.71  
* [новий тег]         v1.72                                 -> v1.72  
* [новий тег]         v1.72b                                -> v1.72b  
* [новий тег]         v1.73                                 -> v1.73  
* [новий тег]         v1.74                                 -> v1.74  
* [новий тег]         v1.75                                 -> v1.75  
* [новий тег]         v1.76                                 -> v1.76  
* [новий тег]         v1.77                                 -> v1.77  
* [новий тег]         v1.78                                 -> v1.78  
* [новий тег]         v1.79                                 -> v1.79  
* [новий тег]         v1.80                                 -> v1.80  
* [новий тег]         v1.81                                 -> v1.81  
* [новий тег]         v1.82                                 -> v1.82  
* [новий тег]         v1.83                                 -> v1.83  
* [новий тег]         v1.84                                 -> v1.84  
* [новий тег]         v1.84.2                               -> v1.84.2  
* [новий тег]         v1.85                                 -> v1.85  
* [новий тег]         v1.86                                 -> v1.86  
* [новий тег]         v1.87                                 -> v1.87  
* [новий тег]         v1.88                                 -> v1.88  
* [новий тег]         v1.89                                 -> v1.89  
* [новий тег]         v1.89.1                               -> v1.89.1  
* [новий тег]         v1.89.2                               -> v1.89.2  
* [новий тег]         v1.89.3                               -> v1.89.3  
* [новий тег]         v1.89.4                               -> v1.89.4  
* [новий тег]         v1.89.5                               -> v1.89.5  
* [новий тег]         v1.89.6                               -> v1.89.6  
* [новий тег]         v1.89.7                               -> v1.89.7  
* [новий тег]         v1.89.7-docking                       -> v1.89.7-docking  
* [новий тег]         v1.89.8                               -> v1.89.8  
* [новий тег]         v1.89.8-docking                       -> v1.89.8-docking  
* [новий тег]         v1.89.9                               -> v1.89.9  
* [новий тег]         v1.89.9-docking                       -> v1.89.9-docking  
* [новий тег]         v1.90                                 -> v1.90  
* [новий тег]         v1.90-docking                         -> v1.90-docking 
* [нова гілка]      main       -> upstream/main  
> git branch -a
* master  
 remotes/origin/HEAD -> origin/master  
 remotes/origin/master  
 remotes/upstream/docking  
 remotes/upstream/features/demo_input_owner_and_routing  
 remotes/upstream/features/potocpav-newer-lines-2  
 remotes/upstream/features/premake5  
 remotes/upstream/features/range_select  
 remotes/upstream/features/shadows  
 remotes/upstream/features/string_view  
 remotes/upstream/features/tex_round_corners  
 remotes/upstream/master
```

```
> git checkout -b lab2-branch
Переключено на нову гілку "lab2-branch"
> nvim main.cpp
> git add main.cpp
> git commit -m "added to main.cpp window creation"
[lab2-branch cf675f85] added to main.cpp window creation  
1 file changed, 5 insertions(+) 
> nvim main.cpp
> git add main.cpp
> git commit -m "added to main.cpp window refresh"
[lab2-branch bdbbb06d] added to main.cpp window refresh  
1 file changed, 9 insertions(+), 1 deletion(-)
> nvim main.cpp
> nvim iteration.h
> git add main.cpp iteration.h
> git commit -m "added function iteration"
[lab2-branch 4d0a0a40] added function iteration  
2 files changed, 7 insertions(+), 1 deletion(-)  
create mode 100644 iteration.h
> git push 
збій: Поточна гілка lab2-branch не має висхідної гілки.  
Щоб надіслати поточну гілку і встановити віддалене призначення першоджерельним сховищем, скористайтесь  
  
   git push --set-upstream origin lab2-branch  
  
Щоб це відбувалося автоматично для невідстежуваних гілок  
першоджерельного сховища, дивіться 'push.autoSetupRemote' у 'git help config'.

> git push origin lab2-branch
Перерахування обʼєктів: 13, готово.  
Підрахунок обʼєктів: 100% (13/13), готово.  
Дельта компресія з використанням до 2 потоків  
Компресія обʼєктів: 100% (10/10), готово.  
Запис обʼєктів: 100% (10/10), 1.13 КіБ | 1.13 МіБ/с, готово.  
Всього 10 (дельта 5), повторно використано 0 (дельта 0), повторно використано пакунків 0  
To file:///home/student/lab2/imgui  
* [new branch]        lab2-branch -> lab2-branch
```

```
> nvim main.cpp
> nvim iteration.h
> git add main.cpp iteration.h
> git commit -m "renamed function iteration -> iteration_text"
[lab2-branch 73f47192] renamed function iteration -> iteration_text  
2 files changed, 2 insertions(+), 2 deletions(-)
> git push -u origin lab2-branch
Перерахування обʼєктів: 10, готово.  
Підрахунок обʼєктів: 100% (10/10), готово.  
Дельта компресія з використанням до 2 потоків  
Компресія обʼєктів: 100% (4/4), готово.  
Запис обʼєктів: 100% (4/4), 458 байтів | 458.00 КіБ/с, готово.  
Всього 4 (дельта 2), повторно використано 0 (дельта 0), повторно використано пакунків 0  
To file:///home/student/lab2/imgui  
  4d0a0a40..73f47192  lab2-branch -> lab2-branch  
гілку "lab2-branch" налаштовано на відстежування "origin/lab2-branch".
```

```
> nvim iteration.h
> git add iteration.h
> git commit -m "edited function iteration"
[lab2-branch a43eb6c8] edited function iteration  
1 file changed, 1 insertion(+)
> git push
Перерахування обʼєктів: 7, готово.  
Підрахунок обʼєктів: 100% (7/7), готово.  
Дельта компресія з використанням до 2 потоків  
Компресія обʼєктів: 100% (3/3), готово.  
Запис обʼєктів: 100% (3/3), 321 байт | 321.00 КіБ/с, готово.  
Всього 3 (дельта 2), повторно використано 0 (дельта 0), повторно використано пакунків 0  
To file:///home/student/lab2/imgui  
  73f47192..a43eb6c8  lab2-branch -> lab2-branch
```

```
> git branch -a
 lab2-branch  
* master  
 remotes/origin/HEAD -> origin/master  
 remotes/origin/master
> git log --graph --all --format="%h %as '%s'" --max-count=10
* a43eb6c 2023-11-30 'edited function iteration'  
* 73f4719 2023-11-30 'renamed function iteration -> iteration_text'  
* 4d0a0a4 2023-11-30 'added function iteration'  
* bdbbb06 2023-11-30 'added to main.cpp window refresh'  
* cf675f8 2023-11-30 'added to main.cpp window creation'  
* 8f570eb 2023-11-30 'added main.cpp'  
* 380bb9d 2023-11-30 'changed function ShowTestWindow'  
* a189812 2023-11-30 'added function ShowTestWindow to imgui_demo.cpp'  
* b112d73 2023-11-29 'Menus: amend to clarify/fix static analyzer warning.  (#6671, #6926)'
```

### 3. Змерджити гілку, що була створена при виконанні ЛР1, в поточну гілку lab2-branch.

```
> git branch -a
 lab2-branch  
* master  
 remotes/origin/HEAD -> origin/master  
 remotes/origin/dev-branch-1  
 remotes/origin/lab2-branch  
 remotes/origin/master  
 remotes/upstream/docking  
 remotes/upstream/features/demo_input_owner_and_routing  
 remotes/upstream/features/potocpav-newer-lines-2  
 remotes/upstream/features/premake5  
 remotes/upstream/features/range_select  
 remotes/upstream/features/shadows  
 remotes/upstream/features/string_view  
 remotes/upstream/features/tex_round_corners  
 remotes/upstream/master
 > git merge origin/dev-branch-1
Оновлення 8f570ebc..ab9b8150  
Fast-forward  
rtd.h | 6 ++++++  
1 file changed, 6 insertions(+)  
create mode 100644 rtd.h
> git log --pretty=oneline --graph
* ab9b815056b02b3601b9d60713b5262c8c82443c (HEAD -> master, origin/dev-branch-1) added rtd.h  
* 8f570ebc67a514009696b3aca9734277e8666cfd (origin/master, origin/HEAD) added main.cpp  
* 380bb9d0ecaabb70f8b4fe9da018789b92d95c75 changed function ShowTestWindow  
* a18981230f858f8d7579eb3649fc3bb15311ec24 added function ShowTestWindow to imgui_demo.cpp  
* b112d73edbf7637a4993baa9a8428dd58c79f45b (grafted, upstream/master) Menus: amend to clarify/fix static analyzer warning.  (#66  
71, #6926)
```

### 4. Перенесення комітів.

```
> git log --graph --pretty=oneline --max-count=13
* 4729433d3950daf2f2482b70ee1b47da99adab8f (HEAD -> lab2-branch-2) added function time_shift
* 83f867458f6bfef5fa84399957e92872dd70375f changed function time
* 81861ad5f1f64a206b1eda7d90ae06eafd47fd51 added time function
* a43eb6c8fd6338e4889c15def017b02e891c2f26 (origin/lab2-branch, lab2-branch) edited function iteration
* 73f47192ea3102e507069d22c33ca1c562d253d3 renamed function iteration -> iteration_text
* 4d0a0a404160a9fed65273b245c4cd21a6de8c90 added function iteration
* bdbbb06d6aa3e01cb9267aa09cc5956bb8adc471 added to main.cpp window refresh
* cf675f85f75d1fda6d9e3ff9a4f0dab0868c99ea added to main.cpp window creation
* 8f570ebc67a514009696b3aca9734277e8666cfd (origin/master, origin/HEAD) added main.cpp
* 380bb9d0ecaabb70f8b4fe9da018789b92d95c75 changed function ShowTestWindow
* a18981230f858f8d7579eb3649fc3bb15311ec24 added function ShowTestWindow to imgui_demo.cpp
* b112d73edbf7637a4993baa9a8428dd58c79f45b (grafted, upstream/master) Menus: amend to clarify/fix static analyzer warning.  (#6671, #6926)
> git checkout lab2-branch
Переключено на гілку "lab2-branch"
Ваша гілка не відрізняється від "origin/lab2-branch".
> git cherry-pick 83f867458f6bfef5fa84399957e92872dd70375f
КОНФЛІКТ (змінено/видалено): timer.h видалено в HEAD та змінено в 83f86745 (changed function time).  Версію 83f86745 (changed function time) з timer.h залишено у дереві.
помилка: не вдалося застосувати 83f86745... changed function time
підказка: Після вирішення конфліктів позначте їх за допомогою
підказка: "git add/rm <визначник шляху>", а потім виконайте
підказка: "git cherry-pick --continue".
підказка: Замість цього ви можете пропустити цей коміт за допомогою "git cherry-pick --skip".
підказка: Щоб перервати процес і повернутися до стану перед "git cherry-pick",
підказка: виконайте "git cherry-pick --abort".
> git add timer.h
> git cherry-pick --continue
[lab2-branch 8401ebc3] changed function time
 Date: Thu Nov 30 23:07:53 2023 +0200
 1 file changed, 7 insertions(+)
 create mode 100644 timer.h
> cat timer.h
#include <time.h>
#include "imgui.h"

void time() {
    ImGui::Text("Time: %f sec", clock()/CLOCKS_PER_SEC);
    ImGui::Spacing();
}
> git log --pretty=oneline --graph -n 10 --branches
* 8401ebc31dbb9d448d94c6eaba0cb145fe343fee (HEAD -> lab2-branch) changed function time
| * 4729433d3950daf2f2482b70ee1b47da99adab8f (lab2-branch-2) added function time_shift
| * 83f867458f6bfef5fa84399957e92872dd70375f changed function time
| * 81861ad5f1f64a206b1eda7d90ae06eafd47fd51 added time function
|/  
* a43eb6c8fd6338e4889c15def017b02e891c2f26 (origin/lab2-branch) edited function iteration
* 73f47192ea3102e507069d22c33ca1c562d253d3 renamed function iteration -> iteration_text
* 4d0a0a404160a9fed65273b245c4cd21a6de8c90 added function iteration
* bdbbb06d6aa3e01cb9267aa09cc5956bb8adc471 added to main.cpp window refresh
* cf675f85f75d1fda6d9e3ff9a4f0dab0868c99ea added to main.cpp window creation
| * ab9b815056b02b3601b9d60713b5262c8c82443c (origin/dev-branch-1, master) added rtd.h
|/  
```

### 5. Визначити останнього спільного предка між двома будь-якими гілками.

```
> git log --pretty=oneline --graph -n 12 --branches
* 8401ebc31dbb9d448d94c6eaba0cb145fe343fee (HEAD -> lab2-branch) changed function time
| * 4729433d3950daf2f2482b70ee1b47da99adab8f (lab2-branch-2) added function time_shift
| * 83f867458f6bfef5fa84399957e92872dd70375f changed function time
| * 81861ad5f1f64a206b1eda7d90ae06eafd47fd51 added time function
|/  
* a43eb6c8fd6338e4889c15def017b02e891c2f26 (origin/lab2-branch) edited function iteration
* 73f47192ea3102e507069d22c33ca1c562d253d3 renamed function iteration -> iteration_text
* 4d0a0a404160a9fed65273b245c4cd21a6de8c90 added function iteration
* bdbbb06d6aa3e01cb9267aa09cc5956bb8adc471 added to main.cpp window refresh
* cf675f85f75d1fda6d9e3ff9a4f0dab0868c99ea added to main.cpp window creation
| * ab9b815056b02b3601b9d60713b5262c8c82443c (origin/dev-branch-1, master) added rtd.h
|/  
* 8f570ebc67a514009696b3aca9734277e8666cfd (origin/master, origin/HEAD) added main.cpp
* 380bb9d0ecaabb70f8b4fe9da018789b92d95c75 changed function ShowTestWindow
> git merge-base origin/lab2-branch origin/dev-branch-1
8f570ebc67a514009696b3aca9734277e8666cfd
```

### 6. Робота з ничкою.

```
> git stash
Збережено робочу директорію та стан індексу WIP on lab2-branch: 8401ebc3 changed function time
> git status
На гілці lab2-branch
Ваша гілка випереджає "origin/lab2-branch" на 1 коміт.
  (скористайтесь "git push", щоб надіслати локальні коміти)

нічого комітити, робоче дерево чисте
> git stash
Збережено робочу директорію та стан індексу WIP on lab2-branch: 8401ebc3 changed function time
> git stash list
stash@{0}: WIP on lab2-branch: 8401ebc3 changed function time
stash@{1}: WIP on lab2-branch: 8401ebc3 changed function time
> git stash apply stash@{1}
На гілці lab2-branch
Ваша гілка випереджає "origin/lab2-branch" на 1 коміт.
  (скористайтесь "git push", щоб надіслати локальні коміти)

Зміни, не додані до майбутнього коміту:
  (скористайтесь "git add <файл>...", щоб оновити майбутній коміт)
  (скористайтесь "git restore <файл>...", щоб скасувати зміни в робочій директорії)
        змінено:       main.cpp

не додано жодних змін до коміту (скористайтесь "git add" та/або "git commit -a")
```

### 7. Робота з файлом .gitignore.

```
> cat .gitignore 
## OSX artifacts
.DS_Store

## Dear ImGui artifacts
imgui.ini

## General build artifacts
*.o
*.obj
*.exe
examples/*/Debug/*
examples/*/Release/*
examples/*/x64/*

## Visual Studio artifacts
.vs
ipch
*.opensdf
*.log
*.pdb
*.ilk
*.user
*.sdf
*.suo
*.VC.db
*.VC.VC.opendb

## Getting files created in JSON/Schemas/Catalog/ from a VS2022 update
JSON/

## Commonly used CMake directories
build*/

## Xcode artifacts
project.xcworkspace
xcuserdata

## Emscripten artifacts
examples/*.o.tmp
examples/*.out.js
examples/*.out.wasm
examples/example_glfw_opengl3/web/*
examples/example_sdl2_opengl3/web/*
examples/example_emscripten_wgpu/web/*

## JetBrains IDE artifacts
.idea
cmake-build-*

## Unix executables from our example Makefiles
examples/example_glfw_metal/example_glfw_metal
examples/example_glfw_opengl2/example_glfw_opengl2
examples/example_glfw_opengl3/example_glfw_opengl3
examples/example_glut_opengl2/example_glut_opengl2
examples/example_null/example_null
examples/example_sdl2_metal/example_sdl2_metal
examples/example_sdl2_opengl2/example_sdl2_opengl2
examples/example_sdl2_opengl3/example_sdl2_opengl3
examples/example_sdl2_sdlrenderer/example_sdl2_sdlrenderer
> nvim testfile.l2
> nvim testfile2.l2
> nvim .gitignore
> cat .gitignore 
## OSX artifacts
.DS_Store

## Dear ImGui artifacts
imgui.ini

## General build artifacts
*.o
*.obj
*.exe
examples/*/Debug/*
examples/*/Release/*
examples/*/x64/*

## Visual Studio artifacts
.vs
ipch
*.opensdf
*.log
*.pdb
*.ilk
*.user
*.sdf
*.suo
*.VC.db
*.VC.VC.opendb

## Getting files created in JSON/Schemas/Catalog/ from a VS2022 update
JSON/

## Commonly used CMake directories
build*/

## Xcode artifacts
project.xcworkspace
xcuserdata

## Emscripten artifacts
examples/*.o.tmp
examples/*.out.js
examples/*.out.wasm
examples/example_glfw_opengl3/web/*
examples/example_sdl2_opengl3/web/*
examples/example_emscripten_wgpu/web/*

## JetBrains IDE artifacts
.idea
cmake-build-*

## Unix executables from our example Makefiles
examples/example_glfw_metal/example_glfw_metal
examples/example_glfw_opengl2/example_glfw_opengl2
examples/example_glfw_opengl3/example_glfw_opengl3
examples/example_glut_opengl2/example_glut_opengl2
examples/example_null/example_null
examples/example_sdl2_metal/example_sdl2_metal
examples/example_sdl2_opengl2/example_sdl2_opengl2
examples/example_sdl2_opengl3/example_sdl2_opengl3
examples/example_sdl2_sdlrenderer/example_sdl2_sdlrenderer

## Lab 2 files
*.l2
> git status
На гілці lab2-branch
Ваша гілка випереджає "origin/lab2-branch" на 1 коміт.
  (скористайтесь "git push", щоб надіслати локальні коміти)

Зміни, не додані до майбутнього коміту:
  (скористайтесь "git add <файл>...", щоб оновити майбутній коміт)
  (скористайтесь "git restore <файл>...", щоб скасувати зміни в робочій директорії)
        змінено:       .gitignore

не додано жодних змін до коміту (скористайтесь "git add" та/або "git commit -a")
> git status --ignored
На гілці lab2-branch
Ваша гілка випереджає "origin/lab2-branch" на 1 коміт.
  (скористайтесь "git push", щоб надіслати локальні коміти)

Зміни, не додані до майбутнього коміту:
  (скористайтесь "git add <файл>...", щоб оновити майбутній коміт)
  (скористайтесь "git restore <файл>...", щоб скасувати зміни в робочій директорії)
        змінено:       .gitignore

Ігноровані файли:
  (скористайтесь "git add -f <файл>...", щоб додати до майбутнього коміту)
        examples/example_null/example_null
        examples/example_null/imgui.ini
        examples/example_null/imgui.o
        examples/example_null/imgui_demo.o
        examples/example_null/imgui_draw.o
        examples/example_null/imgui_tables.o
        examples/example_null/imgui_widgets.o
        examples/example_null/main.o
        testfile.l2
        testfile2.l2

не додано жодних змін до коміту (скористайтесь "git add" та/або "git commit -a")
> git clean -fdx
Видалення examples/example_null/example_null
Видалення examples/example_null/imgui.ini
Видалення examples/example_null/imgui.o
Видалення examples/example_null/imgui_demo.o
Видалення examples/example_null/imgui_draw.o
Видалення examples/example_null/imgui_tables.o
Видалення examples/example_null/imgui_widgets.o
Видалення examples/example_null/main.o
Видалення testfile.l2
Видалення testfile2.l2
```

### 8. Робота з reflog.

```
> git log --pretty=oneline --graph -n 15
* 8401ebc31dbb9d448d94c6eaba0cb145fe343fee (HEAD -> lab2-branch) changed function time
* a43eb6c8fd6338e4889c15def017b02e891c2f26 (origin/lab2-branch) edited function iteration
* 73f47192ea3102e507069d22c33ca1c562d253d3 renamed function iteration -> iteration_text
* 4d0a0a404160a9fed65273b245c4cd21a6de8c90 added function iteration
* bdbbb06d6aa3e01cb9267aa09cc5956bb8adc471 added to main.cpp window refresh
* cf675f85f75d1fda6d9e3ff9a4f0dab0868c99ea added to main.cpp window creation
* 8f570ebc67a514009696b3aca9734277e8666cfd (origin/master, origin/HEAD) added main.cpp
* 380bb9d0ecaabb70f8b4fe9da018789b92d95c75 changed function ShowTestWindow
* a18981230f858f8d7579eb3649fc3bb15311ec24 added function ShowTestWindow to imgui_demo.cpp
* b112d73edbf7637a4993baa9a8428dd58c79f45b (grafted, upstream/master) Menus: amend to clarify/fix static analyzer warning.  (#6671, #6926)
> git checkout master
Переключено на гілку "master"
Ваша гілка випереджає "origin/master" на 1 коміт.
  (скористайтесь "git push", щоб надіслати локальні коміти)
> git branch -D lab2-branch
Видалено гілку lab2-branch (була 00b98139).
> git push -d origin lab2-branch
To file:///home/student/lab2/imgui
 - [deleted]           lab2-branch
> git reflog
ab9b8150 (HEAD -> master, origin/dev-branch-1) HEAD@{0}: checkout: moving from lab2-branch to master
00b98139 HEAD@{1}: commit (amend): changed function time
8401ebc3 HEAD@{2}: checkout: moving from master to lab2-branch
ab9b8150 (HEAD -> master, origin/dev-branch-1) HEAD@{3}: checkout: moving from lab2-branch to master
8401ebc3 HEAD@{4}: reset: moving to HEAD
8401ebc3 HEAD@{5}: reset: moving to HEAD
8401ebc3 HEAD@{6}: reset: moving to HEAD
8401ebc3 HEAD@{7}: reset: moving to HEAD
8401ebc3 HEAD@{8}: commit (cherry-pick): changed function time
a43eb6c8 HEAD@{9}: checkout: moving from lab2-branch-2 to lab2-branch
4729433d (lab2-branch-2) HEAD@{10}: checkout: moving from lab2-branch to lab2-branch-2
a43eb6c8 HEAD@{11}: checkout: moving from lab2-branch-2 to lab2-branch
4729433d (lab2-branch-2) HEAD@{12}: commit: added function time_shift
83f86745 HEAD@{13}: commit: changed function time
81861ad5 HEAD@{14}: commit: added time function
a43eb6c8 HEAD@{15}: checkout: moving from lab2-branch to lab2-branch-2
a43eb6c8 HEAD@{16}: checkout: moving from master to lab2-branch
ab9b8150 (HEAD -> master, origin/dev-branch-1) HEAD@{17}: merge origin/dev-branch-1: Fast-forward
8f570ebc (origin/master, origin/HEAD) HEAD@{18}: checkout: moving from dev-branch-1 to master
60243c7f HEAD@{19}: commit: added rtd.h
a43eb6c8 HEAD@{20}: checkout: moving from lab2-branch to dev-branch-1
a43eb6c8 HEAD@{21}: commit: edited function iteration
73f47192 HEAD@{22}: commit: renamed function iteration -> iteration_text
4d0a0a40 HEAD@{23}: reset: moving to HEAD~1
1a30795d HEAD@{24}: commit: renamed function iteration -> iteration_text
4d0a0a40 HEAD@{25}: commit: added function iteration
bdbbb06d HEAD@{26}: commit: added to main.cpp window refresh
cf675f85 HEAD@{27}: commit: added to main.cpp window creation
8f570ebc (origin/master, origin/HEAD) HEAD@{28}: checkout: moving from master to lab2-branch
8f570ebc (origin/master, origin/HEAD) HEAD@{29}: clone: from file:///home/student/lab2/imgui
> git branch lab2-resurrected 8401ebc3
> git checkout lab2-resurrected
Переключено на гілку "lab2-resurrected"
> git log --pretty=oneline --graph -n 15
* 8401ebc31dbb9d448d94c6eaba0cb145fe343fee (HEAD -> lab2-resurrected) changed function time
* a43eb6c8fd6338e4889c15def017b02e891c2f26 edited function iteration
* 73f47192ea3102e507069d22c33ca1c562d253d3 renamed function iteration -> iteration_text
* 4d0a0a404160a9fed65273b245c4cd21a6de8c90 added function iteration
* bdbbb06d6aa3e01cb9267aa09cc5956bb8adc471 added to main.cpp window refresh
* cf675f85f75d1fda6d9e3ff9a4f0dab0868c99ea added to main.cpp window creation
* 8f570ebc67a514009696b3aca9734277e8666cfd (origin/master, origin/HEAD) added main.cpp
* 380bb9d0ecaabb70f8b4fe9da018789b92d95c75 changed function ShowTestWindow
* a18981230f858f8d7579eb3649fc3bb15311ec24 added function ShowTestWindow to imgui_demo.cpp
* b112d73edbf7637a4993baa9a8428dd58c79f45b (grafted, upstream/master) Menus: amend to clarify/fix static analyzer warning.  (#6671, #6926)
```